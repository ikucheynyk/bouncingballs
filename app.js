$(document).ready(function() {

    /**
     * Ball model class
     */
    var BallModel = Backbone.Model.extend({
        defaults: {
            radius: 50,
            speedX: 10,
            speedY: 10,
            // Position of the ball on the screen
            x: 100,
            y: 100,
            color: 'red',
            attenuation: 10,
            gravity: 10,
            stroke: '',
            strokeWidth: 0
        }
    });

    /**
     * Ball view class
     * It does actual move and bump
     */
    var BallView = Backbone.View.extend({
        model: new BallModel(),
        initialize: function() {
            // Binding the data to view,
            // immediately change shape attributes when model properties changed
            this.model.bind('change', this.update, this)
        },
        update: function() {
            this.el.setAttribute('r', this.model.get('radius'));
            this.el.setAttribute('fill', this.model.get('color'));
            this.el.setAttribute('cx', this.model.get('x'));
            this.el.setAttribute('cy', this.model.get('y'));
            this.el.setAttribute('stroke', this.model.get('stroke'));
            this.el.setAttribute('stroke-width', this.model.get('strokeWidth'));
            $('.panel ul li[objectid=' + this.model.cid + ']').text('R' + this.model.get('radius'));
        },
        render: function() {
            this.scene = $('#scene')[0];
            this.sceneWidth = $(this.scene).width();
            this.sceneHeight = $(this.scene).height();
            this.dragging = false;
            this.el = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
            this.width = $(this.el).width();
            this.height = $(this.el).height();

            // These events help to drag and drop the model
            // While dragging it changes the speed of shape
            // so that after release it continue to move
            var self = this;
            var mouseX, mouseY, keepX, keepY;
            $(document).mousemove(function(ev) {
                mouseX = ev.offsetX;
                mouseY = ev.offsetY;
                if(self.dragging) {
                    // Move shape as much as mouse moved (keep pointer in the center)
                    self.model.set('x', mouseX);
                    self.model.set('y', mouseY);
                    // Calculate dragging speed
                    self.model.set('speedX', (keepX - mouseX) * -1);
                    self.model.set('speedY', (keepY - mouseY) * -1);
                    keepX = mouseX;
                    keepY = mouseY;
                }
            });
            $(this.el).mousedown(function(ev) {
                self.dragging = true;
                keepX = mouseX;
                keepY = mouseY;
                // Catch the shape in the center
                self.model.set('x', mouseX);
                self.model.set('y', mouseY);
            });
            $(this.el).mouseup(function(ev) {
                self.dragging = false;
                self.move();
            });
            var mouseOutTimeout;
            $(this.el).mouseout(function(ev) {
                mouseOutTimeout = setTimeout(function() {
                    if(self.dragging) {
                        self.dragging = false;
                        self.move();
                    }
                }, 50);
            });
            $(this.el).mouseover(function(ev) {
                clearTimeout(mouseOutTimeout);
            });
            this.move();
            return this;
        },
        // This function is doing a move and bump
        move: function() {
            if(this.dragging) {
                return;
            }
            if(this.model.get('gravity')!=0) {
                this.model.set('speedY', this.model.get('speedY') ? this.model.get('speedY') : 1);
                this.model.set('speedY', this.model.get('speedY') + (Math.abs(this.model.get('speedY'))/100 * this.model.get('gravity')));
                if(this.model.get('speedY') < 1 && this.model.get('speedY') > -1) {
                    this.model.set('speedY', 0);
                }
                if(this.model.get('speedX') < 1 && this.model.get('speedX') > -1) {
                    this.model.set('speedX', 0);
                }
            }
            this.model.set('x', this.model.get('x') + this.model.get('speedX'));
            this.model.set('y', this.model.get('y') + this.model.get('speedY'));
            // Check if it had reached the top
            if(this.model.get('y')-this.model.get('radius') < 0) {
                // Set it on the bottom
                this.model.set('y', this.model.get('radius'));
                this.model.set('speedY', this.model.get('speedY') * -1 + ((Math.abs(this.model.get('speedY'))/100 * this.model.get('attenuation'))));
            }
            // Check if it had reached the bottom
            if(this.model.get('y')+this.model.get('radius') > this.sceneHeight) {
                // Set it on the bottom
                this.model.set('y', this.sceneHeight - this.model.get('radius'));
                this.model.set('speedY', this.model.get('speedY') * -1 + ((Math.abs(this.model.get('speedY'))/100 * this.model.get('attenuation'))));
                this.model.set('speedX', this.model.get('speedX') + ((-1*this.model.get('speedX')/100) * 0.5));
            }
            // Check if it had reached the right side
            if(this.model.get('x')+this.model.get('radius') > this.sceneWidth) {
                // Set it on the bottom
                this.model.set('x', this.sceneWidth - this.model.get('radius'));
                this.model.set('speedX', this.model.get('speedX') * -1 + ((Math.abs(this.model.get('speedX'))/100 * this.model.get('attenuation'))));
            }
            // Check if it had reached the left side
            if(this.model.get('x')-this.model.get('radius') < 0) {
                // Set it on the bottom
                this.model.set('x', this.model.get('radius'));
                this.model.set('speedX', this.model.get('speedX') * -1 + ((Math.abs(this.model.get('speedX'))/100 * this.model.get('attenuation'))));
            }
            // Check bumped
            var oShape = this.getBumpedShape();
            if(oShape) {
                var spX = this.model.get('speedX');
                var spY = this.model.get('speedY');
                this.model.set('speedX', oShape.get('speedX'));
                this.model.set('speedY', oShape.get('speedY'));
                this.model.set('speedX', this.model.get('speedX') + ((-1*this.model.get('speedX')/100) * 0.5));
                this.model.set('speedY', this.model.get('speedY') + ((-1*this.model.get('speedY')/100) * 0.5));
                oShape.set('speedX', spX);
                oShape.set('speedY', spY);
                oShape.set('speedX', oShape.get('speedX') + ((-1*oShape.get('speedX')/100) * 0.5));
                oShape.set('speedY', oShape.get('speedY') + ((-1*oShape.get('speedY')/100) * 0.5));
            }
            var self = this;
            setTimeout(function(){
                self.move();
            }, 10);
        },
        getBumpedShape: function() {
            var length = this.model.collection.models.length;
            var that;
            for(var ii=0; ii<length; ii++) {
                that = this.model.collection.models[ii];
                if(that!=this.model) {
                    // determine if those 2 shapes are bumped
                    if(this.model.get('radius') + that.get('radius') > Math.sqrt(Math.pow(Math.abs(this.model.get('x') - that.get('x')), 2) + Math.pow(Math.abs(this.model.get('y') - that.get('y')), 2))) {
                        return that;
                    }
                }
            }
            return null;
        }
    });

    /**
     * Just a collection of Balls
     * Its used as a storage so that balls can find each other and interact
     */
    var BallsCollection = Backbone.Collection.extend({
        model: BallModel,
        url: '#'
    });

    /**
     * Main application logic
     */
    var AppView = Backbone.View.extend({
        // Balls collection initalized
        // it has 2 initial balls that will bump upon page load
        balls: new BallsCollection([{
            radius: 50,
            x: 100,
            y: 200,
            speedX: 10,
            speedY: 15,
            color: 'green'
        },{
            radius: 100,
            x: 300,
            y: 300,
            color: 'blue',
            speedX: 15,
            speedY: 10
        }]),
        el: $('#scene'),
        initialize: function() {
            this.balls.on('add', function(ball) {
                this.renderBall(ball);
            }, this);
            this.render();
        },
        render: function() {
            var that = this;
            // Renders each ball
            this.balls.each(function(ball) {
                that.renderBall(ball);
            });
            // Set the position of panel to the right bottom corner when window resized
            $(window).resize(function() {
                $('.panel').offset({
                    top: $('svg').height() - $('.panel').height(),
                    left: $('svg').width() - $('.panel').width()
                });
            });
            $(window).trigger('resize');
            // When clicking on the `New` shape elemet, prepare the form
            $('.panel ul li:last-child').click(function() {
                $('.panel ul li').removeClass('selected');
                $(this).addClass('selected');
                $('.panel form select[name=color]').val('');
                $('.panel form input[name=radius]').val('');
                $('.panel form input[name=attenuation]').val('');
                $('.panel form input[name=gravity]').val('');
                $('.panel form input[name=speedX]').val('');
                $('.panel form input[name=speedY]').val('');
                $('.panel form input[name=x]').val('');
                $('.panel form input[name=y]').val('');
                $('.panel form input[name=cid]').val('');
                $('.panel form button').text('Create');
                // Remove strokes from all the balls
                that.balls.each(function(ball) {
                    ball.set('stroke', '');
                    ball.set('strokeWidth', '');
                });
            });
            $('.panel a.more').click(function() {
                if($(this).text()=='More') {
                    $(this).text('Less');
                    $('.panel form input[name=useextended]').val('true');
                } else {
                    $(this).text('More');
                    $('.panel form input[name=useextended]').val('false');
                }
                $('.panel .expandable').slideToggle({
                    progress: function() {
                        $(window).trigger('resize');
                    },
                    complete: function() {
                        $(window).trigger('resize');
                    }
                });
            });
            $('.panel form ').submit(function() {
                var data = {
                    color: $(this).find('select[name=color]').val(),
                    radius: Math.round($(this).find('input[name=radius]').val()),
                    gravity: Math.round($(this).find('input[name=gravity]').val())
                };
                if($(this).find('input[name=useextended]').val()=='true') {
                    data.attenuation = Math.round($(this).find('input[name=attenuation]').val());
                    data.speedX = Math.round($(this).find('input[name=speedX]').val());
                    data.speedY = Math.round($(this).find('input[name=speedY]').val());
                    data.x = Math.round($(this).find('input[name=x]').val());
                    data.y = Math.round($(this).find('input[name=y]').val());
                }
                // Check that at least radius and color are available
                if(Math.round(data.radius)==0) {
                    alert('Please provide radius');
                    return false;
                }
                if(Math.round(data.color)=='') {
                    alert('Please choose color');
                    return false;
                }
                var cid = $(this).find('input[name=cid]').val();
                if(cid) {
                    that.balls.get(cid).set(data);
                } else {
                    that.balls.add(new BallModel(data));
                    $(this).find('select[name=color]').val('');
                    $(this).find('input[name=radius]').val('');
                    $(this).find('input[name=attenuation]').val('');
                    $(this).find('input[name=gravity]').val('');
                    $(this).find('input[name=speedX]').val('');
                    $(this).find('input[name=speedY]').val('');
                    $(this).find('input[name=x]').val('');
                    $(this).find('input[name=y]').val('');
                    $(this).find('input[name=cid]').val('');
                }
                return false;
            });
            // Make sure the controls at the right position
            $(window).trigger('resize');
        },
        renderBall: function(ball) {
            var that = this;
            var ballView = new BallView({
                model: ball
            });
            this.el.appendChild(ballView.render().el);
            var item = $('<li></li>').text(
            'R' + ball.get('radius')).attr('objectId', ball.cid).click(function(){
                that.selectBall(this, $(this).attr('objectId'));
            }).css('background-color', ball.get('color'));
            $('.panel ul li:last-child').before(item);
            $(window).trigger('resize');
        },
        // Selects the ball in the list of balls on the right screen
        // and makes it available to edit
        selectBall: function(context, cid) {
            if(cid) {
                var ball = this.balls.get(cid);
                $('.panel form button').text('Save');
            } else {
                var ball = new BallModel();
            }
            // Highlight selected ball
            this.balls.each(function(ball) {
                ball.set('stroke', '');
                ball.set('strokeWidth', '');
            });
            ball.set('stroke', 'red');
            ball.set('strokeWidth', '3');
            $('.panel ul li').removeClass('selected');
            $(context).addClass('selected');

            $('.panel form select[name=color]').val(ball.get('color'));
            $('.panel form input[name=radius]').val(ball.get('radius'));
            $('.panel form input[name=attenuation]').val(ball.get('attenuation'));
            $('.panel form input[name=gravity]').val(ball.get('gravity'));
            $('.panel form input[name=speedX]').val(ball.get('speedX'));
            $('.panel form input[name=speedY]').val(ball.get('speedY'));
            $('.panel form input[name=x]').val(ball.get('x'));
            $('.panel form input[name=y]').val(ball.get('y'));
            $('.panel form input[name=cid]').val(ball.cid);
        }
    });

    // Start the application
    var app = new AppView();
});